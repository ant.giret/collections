import '@/registerServiceWorker';
import 'vue-material/dist/theme/default-dark.css';
import 'vue-material/dist/vue-material.min.css';

import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import Vue from 'vue';
import {
  MdApp,
  MdAutocomplete,
  MdAvatar,
  MdButton,
  MdCard,
  MdCheckbox,
  MdContent,
  MdDivider,
  MdDialog,
  MdDialogConfirm,
  MdDrawer,
  MdEmptyState,
  MdField,
  MdIcon,
  MdList,
  MdMenu,
  MdProgress,
  MdRadio,
  MdSubheader,
  MdToolbar,
  MdTooltip,
} from 'vue-material/dist/components';

Vue.use(MdApp);
Vue.use(MdAutocomplete);
Vue.use(MdAvatar);
Vue.use(MdButton);
Vue.use(MdCard);
Vue.use(MdCheckbox);
Vue.use(MdContent);
Vue.use(MdDivider);
Vue.use(MdDialog);
Vue.use(MdDialogConfirm);
Vue.use(MdDrawer);
Vue.use(MdEmptyState);
Vue.use(MdField);
Vue.use(MdIcon);
Vue.use(MdList);
Vue.use(MdMenu);
Vue.use(MdProgress);
Vue.use(MdRadio);
Vue.use(MdSubheader);
Vue.use(MdToolbar);
Vue.use(MdTooltip);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
