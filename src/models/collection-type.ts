export enum CollectionTypes {
  Audio = 1,
  Video,
  Books,
  Other,
}

interface CollectionType {
  value: CollectionTypes;
  code: string;
  title: string;
  icon: string;
}

export const collectionTypes = [
  CollectionTypes.Audio,
  CollectionTypes.Video,
  CollectionTypes.Books,
  CollectionTypes.Other,
];

export const collectionTypesMap: { [key: number]: CollectionType } = {
  [CollectionTypes.Audio]: {
    value: CollectionTypes.Audio,
    code: 'AUDIO',
    title: 'Audio',
    icon: 'library_music',
  },
  [CollectionTypes.Video]: {
    value: CollectionTypes.Video,
    code: 'VIDEO',
    title: 'Video',
    icon: 'video_library',
  },
  [CollectionTypes.Books]: {
    value: CollectionTypes.Books,
    code: 'BOOKS',
    title: 'Books',
    icon: 'library_books',
  },
  [CollectionTypes.Other]: {
    value: CollectionTypes.Other,
    code: 'OTHER',
    title: 'Other',
    icon: 'collections_bookmark',
  },
};

export function toCollectionType(code: string): CollectionTypes {
  const key = Object.keys(collectionTypesMap).find((k: string) => collectionTypesMap[k].code === code);
  return key ? parseInt(key, 10) : CollectionTypes.Other;
}
