import firebase from 'firebase/app';
import { Item } from './item';

export class AudioItem extends Item {
  constructor(
    title: string,
    created: Date,
    public readonly artists: string[],
    photoUrl: string | null,
    tags: string[],
  ) {
    super(title, created, photoUrl, tags);
  }

  public toObject(): { [key: string]: any } {
    return {
      title: this.title,
      created: firebase.firestore.Timestamp.fromDate(this.created),
      artists: this.artists,
      photo_url: this.photoUrl,
      tags: this.tags,
    };
  }
}

export function toAudioItem(data: any): Item {
  return new AudioItem(
    data.title,
    data.created ? new Date(data.created.seconds * 1000) : new Date(),
    data.artists || [],
    data.photo_url,
    data.tags || [],
  );
}
