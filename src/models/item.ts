import firebase from 'firebase/app';

export class Item {
  constructor(
    public readonly title: string,
    public readonly created: Date,
    public photoUrl: string | null,
    public readonly tags: string[],
  ) {}

  public toObject(): { [key: string]: any } {
    return {
      title: this.title,
      created: firebase.firestore.Timestamp.fromDate(this.created),
      photo_url: this.photoUrl,
      tags: this.tags,
    };
  }
}

export function toItem(data: any): Item {
  return new Item(
    data.title,
    data.created ? new Date(data.created.seconds * 1000) : new Date(),
    data.photo_url,
    data.tags || [],
  );
}
