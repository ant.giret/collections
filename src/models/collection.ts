import firebase from 'firebase/app';
import { CollectionTypes, collectionTypesMap, toCollectionType } from './collection-type';
import { Item, toItem } from './item';
import { toAudioItem } from './audio-item';

export class Collection {
  constructor(
    public readonly id: string | null = null,
    public readonly type: CollectionTypes | null = null,
    public readonly title: string = '',
    public readonly created: Date = new Date(),
    public readonly updated: Date = new Date(),
    public readonly items: Item[] = [],
  ) {}

  public get isAudio(): boolean {
    return this.type === CollectionTypes.Audio;
  }

  public toObject(): { [key: string]: any } {
    return {
      type: collectionTypesMap[this.type as CollectionTypes].code,
      title: this.title,
      created: firebase.firestore.Timestamp.fromDate(this.created),
      updated: firebase.firestore.Timestamp.fromDate(this.updated),
      items: this.items,
    };
  }

  public toLowerCase(): string {
    return this.title.toLowerCase();
  }

  public toString(): string {
    return this.title;
  }
}

export function toCollection(id: string, data: any): Collection {
  const type = toCollectionType(data.type);
  return new Collection(
    id,
    type,
    data.title,
    data.created ? new Date(data.created.seconds * 1000) : new Date(),
    data.updated ? new Date(data.updated.seconds * 1000) : new Date(),
    (data.items || []).map((itemData: any) => {
      switch (type) {
        case CollectionTypes.Audio:
          return toAudioItem(itemData);
        default:
          return toItem(itemData);
      }
    }),
  );
}
