import Vue from 'vue';
import Router from 'vue-router';

import CollectionDetail from '@/components/CollectionDetail.vue';
import CollectionList from '@/components/CollectionList.vue';

Vue.use(Router);

Vue.component('router-link', (Vue as any).options.components.RouterLink);
Vue.component('router-view', (Vue as any).options.components.RouterView);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/collections/:id',
      name: 'collection',
      component: CollectionDetail,
    },
    {
      path: '/collections',
      name: 'collections',
      component: CollectionList,
    },
    {
      path: '**',
      redirect: { name: 'collections' },
    },
  ],
});
