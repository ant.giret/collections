import firebase from 'firebase/app';
import 'firebase/firestore';

import { Collection, toCollection } from '@/models/collection';

firebase.initializeApp({
  apiKey: 'AIzaSyCILG8MRd0Bo2G81-6yXbXMpMYsdlSuGww',
  authDomain: 'collections-1b9c2.firebaseapp.com',
  databaseURL: 'https://collections-1b9c2.firebaseio.com',
  projectId: 'collections-1b9c2',
  storageBucket: 'collections-1b9c2.appspot.com',
  messagingSenderId: '24674688241',
});

const db = firebase.firestore();
const settings = { timestampsInSnapshots: true };
db.settings(settings);

const collections = db.collection('collections');

export default {
  namespaced: true,
  state: {
    areCollectionsLoaded: false,
    collections: [],
  },
  mutations: {
    collections(state) {
      collections.onSnapshot((snapshot) => {
        state.collections = (snapshot.docs || []).map((doc: any) => toCollection(doc.id, doc.data()));
        state.areCollectionsLoaded = true;
      });
    },
    addCollection(state, collection: Collection) {
      collections.add(collection.toObject());
    },
  },
};
